#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

class ShaderManager
{
public:
	ShaderManager();
	~ShaderManager();

	std::string readFile(const char *filePath);
	unsigned int loadShader(const char *vertexPath, const char *fragmentPath);
};

