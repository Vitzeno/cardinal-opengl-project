#include "DisplayManager.h"
#include "ShaderManager.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>


/* Vertex data for a basic triangle */
float vertices[] = {
	 0.5f,  0.5f, 0.0f,  // top right
	 0.5f, -0.5f, 0.0f,  // bottom right
	-0.5f, -0.5f, 0.0f,  // bottom left
	-0.5f,  0.5f, 0.0f   // top left 
};
unsigned int indices[] = {  // note that we start from 0!
	0, 1, 3,			// first triangle
	1, 2, 3				// second triangle
};


int main()
{
	//Initialse GLFW before it can be used
	glfwInit();
	//Set OpenGL version to 3.3 (will fail to run if this version is not suppoerted by user GPU)
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	//Explicitly tell GLFW to use core mode (therefore cannot use older functions in immediate mode)
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	DisplayManager* diaplayManager = DisplayManager::getInstance();
	GLFWwindow* window = diaplayManager->createDisplay();

	//Initialise GLAD before calling any OpenGL functions
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "ERROR: Failed to initialize GLAD" << std::endl;
		return -1;
	}

	/* Create a VBO */
	unsigned int VBO;
	glGenBuffers(1, &VBO);
	/* VBO must be bound to specific buffer type */
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	/* Copy buffer data to GPU memeory requires buffer type, size of data, actual data, memory usage */
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	/* Create VAO */
	unsigned int VAO;
	glGenVertexArrays(1, &VAO);
	/* Always bind before use */
	glBindVertexArray(VAO);
	/* Which VAO to use, 0 in this case*/
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	/* Instead of unbinding, just bind VAO 0 */
	glEnableVertexAttribArray(0);

	/* Create EBO */
	unsigned int EBO;
	glGenBuffers(1, &EBO);
	/* EBO must be bound to specific buffer type */
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	/* Copy buffer data to GPU memeory requires buffer type, size of data, actual data, memory usage */
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);


	/* Shader manager reads and compiles shaders then returns a shader program for use */
	ShaderManager* shaderManager = new ShaderManager();
	unsigned int shaderProgram = shaderManager->loadShader("VertexShader.glsl", "FragmentShader.glsl");
	

	
	//Determines if windows has been instructed to close, if so loop ends
	while (!glfwWindowShouldClose(window))
	{
		//Handle some inputs
		diaplayManager->processInputs(window);


		//FUN FUN RENDERING HERE
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glUseProgram(shaderProgram);
		glBindVertexArray(VAO);
		//glDrawArrays(GL_TRIANGLES, 0, 3);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		//Swaps front and back buffers to prevent artifacts (double buffering)
		glfwSwapBuffers(window);
		//Checks events such as inputs, which are handled by call back functions
		glfwPollEvents();
	}

	/* Delete VAO and VBO when not needed */
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);

	//Clean up and quit
	glfwTerminate();
	return 0;
}

