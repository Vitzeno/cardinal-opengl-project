#include "DisplayManager.h"


DisplayManager::DisplayManager()
{
}

DisplayManager::~DisplayManager()
{
}

/* Null, because instance will be initialized on demand */
DisplayManager* DisplayManager::instance = 0;

DisplayManager * DisplayManager::getInstance()
{
	if (instance == 0)
	{
		instance = new DisplayManager();
	}

	return instance;
}

/*
Creates window and registers call back function for window resizing
*/
GLFWwindow * DisplayManager::createDisplay()
{
	//Window creation
	GLFWwindow * window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Cardinal", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "ERROR: Failed to create window" << std::endl;
		glfwTerminate();
	}
	glfwMakeContextCurrent(window);

	glfwSetFramebufferSizeCallback(window, resizeWindow);

	return window;
}

void DisplayManager::resizeWindow(GLFWwindow * window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void DisplayManager::processInputs(GLFWwindow * window)
{
	/* Close window */
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
	/* Wireframe mode */
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	/* Normal mode */
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}







