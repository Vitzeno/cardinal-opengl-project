#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>

class DisplayManager
{
public:
	~DisplayManager();

	/* static access method for instace */
	static DisplayManager* getInstance();

	static GLFWwindow * createDisplay();
	static void resizeWindow(GLFWwindow * window, int width, int height);
	static void processInputs(GLFWwindow * window);

private:
	/* Single instance of class */
	static DisplayManager* instance;

	/* Private constructor to make class singleton */
	DisplayManager();

	//static GLFWwindow * window;
	static const int SCR_WIDTH = 1920;
	static const int SCR_HEIGHT = 1080;
};

